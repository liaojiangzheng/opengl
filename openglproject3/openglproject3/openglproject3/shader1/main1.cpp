////
////  main.cpp
////  openglproject3
////
////  Created by 廖江正 on 2020/12/7.
////  Copyright © 2020 廖江正. All rights reserved.
////
//
//#include "shader1.hpp"
////#include "test.hpp"
//void framebuffer_size_callback(GLFWwindow* window, int width, int height);//回调函数原型声明
//void processInput(GLFWwindow *window);
//
//// settings
//const unsigned int SCR_WIDTH = 800;
//const unsigned int SCR_HEIGHT = 600;
//
////输入控制，检查用户是否按下了返回键(Esc)
//void processInput(GLFWwindow *window)
//{
//    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
//        glfwSetWindowShouldClose(window, true);
//}
//
//// 当用户改变窗口的大小的时候，视口也应该被调整
//void framebuffer_size_callback(GLFWwindow* window, int width, int height)
//{
//    // 注意：对于视网膜(Retina)显示屏，width和height都会明显比原输入值更高一点。
//    glViewport(0, 0, width, height);
//}
//
//int main(int argc, char * argv[]) {
//
//    //初始化GLFW
//    glfwInit();
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
//#ifdef __APPLE__
//    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
//#endif
//
//    //创建一个窗口对象
//    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "FirstGL", NULL, NULL);
//    if (window == NULL)
//    {
//        std::cout << "Failed to create GLFW window" << std::endl;
//        glfwTerminate();
//        return -1;
//    }
//
//    //通知GLFW将我们窗口的上下文设置为当前线程的主上下文
//    glfwMakeContextCurrent(window);
//
//    //对窗口注册一个回调函数,每当窗口改变大小，GLFW会调用这个函数并填充相应的参数供你处理
//    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
//
//    //初始化GLAD用来管理OpenGL的函数指针
//    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
//    {
//        std::cout << "Failed to initialize GLAD" << std::endl;
//        return -1;
//    }
//
////    Shader1* ourshader = new Shader1("openglproject3/shader1/shadersrc/vertexshader1.txt", "shader1/shadersrc/fragmentshader1.txt");
//    Shader1* ourshader = new Shader1("/Users/liaojiangzheng/work/opengl/openglproject3/openglproject3/openglproject3/shader1/shadersrc/vertexshader1.txt", "/Users/liaojiangzheng/work/opengl/openglproject3/openglproject3/openglproject3/shader1/shadersrc/fragmentshader1.txt");
//
//
//   GLfloat vertices1[] = {
//        // 位置              // 颜色
//         0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,   // 右下
//        -0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,   // 左下
//         0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f    // 顶部
//    };
//
//
//    GLuint indices1[] = { // 注意索引从0开始!
//        0, 1, 2, // 第一个三角形
//    };
//
//
//    //*
//    GLuint VAO;
//    //生成并绑定顶点缓冲对象 vertex buffer objects
//
//    //*
//    glGenVertexArrays(1, &VAO);//function name 有 s 说明是可以生成多个的，这里只有一个，所以直接取地址作为第一个元素。
//    // 1. 绑定VAO
//    glBindVertexArray(VAO);
//    // 1. 复制顶点数组到缓冲中供OpenGL使用
//    GLuint VBO;
//    glGenBuffers(1, &VBO);
//    glBindBuffer(GL_ARRAY_BUFFER, VBO);
//
//    //从这一刻起，我们使用的任何（在GL_ARRAY_BUFFER目标上的）缓冲调用都会用来配置当前绑定的缓冲(VBO)。然后我们可以调用glBufferData函数，它会把之前定义的顶点数据复制到缓冲的内存中：
//    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);
//
//    GLuint EBO;
//    glGenBuffers(1, &EBO);
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices1), indices1, GL_STATIC_DRAW);
//
//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
//    //这里的第一个参数0就是和shader代码里面的location关联的。
//    glEnableVertexAttribArray(0);//必须要开启
//    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
//    glEnableVertexAttribArray(1);//必须要开启
//
//
////    glBindBuffer(GL_ARRAY_BUFFER, 0);
//    glBindVertexArray(0);
//
//
//    GLint nrAttributes;
//    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
//    std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;
//
//    //渲染循环
//    while(!glfwWindowShouldClose(window))
//    {
//        // 输入
//        processInput(window);
//
//        // 渲染指令
//        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
//        glClear(GL_COLOR_BUFFER_BIT);
//
//
//
//
////在CPU里面去设置uniform变量的值，循环设置，从而在渲染的时候会得到不同的值。
////        GLfloat timeValue = glfwGetTime();
////        GLfloat greenValue = (sin(timeValue) / 2) + 0.5;
////        GLint vertexColorLocation = glGetUniformLocation(ourshader->Program, "uniformColor");
////
////        glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f);
//        ourshader->Use();
//        // 2. 当我们渲染一个物体时要使用着色器程序
////        glUseProgram(shaderProgram1);
//        glBindVertexArray(VAO);
//        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
////        glBindBuffer(GL_ARRAY_BUFFER, VBO);
////        glDrawArrays(GL_TRIANGLES, 0, 3);
//
//
////        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
////        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);
//
//        glDrawArrays(GL_TRIANGLES, 0, 3);
//        glBindVertexArray(0);
//
//        // 检查并调用事件，交换缓冲
//        glfwSwapBuffers(window);//检查触发事件
//        glfwPollEvents();    //交换颜色缓冲
//    }
//
//
//    glDeleteVertexArrays(1, &VAO);
//    glDeleteBuffers(1, &VBO);
//    // Terminate GLFW, clearing any resources allocated by GLFW.
//
//    //释放/删除之前的分配的所有资源
//    glfwTerminate();
//    return EXIT_SUCCESS;
//}
