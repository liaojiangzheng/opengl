////
////  main.m
////  openglproject3
////
////  Created by 廖江正 on 2020/11/21.
////  Copyright © 2020 廖江正. All rights reserved.
////
//
////#include <stdint.h>
//
//
////#include <iostream>
////int main(int argc, char * argv[]) {
////    std::cout << "Failed to create GLFW window" << std::endl;
////    return 0;
////}
////*
//
//
//// System Headers
//#include <glad/glad.h>
//#include <GLFW/glfw3.h>
//
//// Standard Headers
//#include <cstdio>
//#include <cstdlib>
//#include <iostream>
//
///*
//
// 标准化设备坐标(Normalized Device Coordinates, NDC)
//一旦你的顶点坐标已经在顶点着色器中处理过，它们就应该是标准化设备坐标了
// 准化设备坐标是一个x、y和z值在-1.0到1.0的一小段空间。
// 任何落在范围外的坐标都会被丢弃/裁剪，不会显示在你的屏幕上。
// 与通常的屏幕坐标不同，y轴正方向为向上，(0,0)坐标是这个图像的中心，而不是左上角。最终你希望所有(变换过的)坐标都在这个坐标空间中，否则它们就不可见了。
// 你的标准化设备坐标接着会变换为屏幕空间坐标(Screen-space Coordinates)，这是使用你通过glViewport函数提供的数据，进行视口变换(Viewport Transform)完成的。所得的屏幕空间坐标又会被变换为片段输入到片段着色器中。
//
// */
//void framebuffer_size_callback(GLFWwindow* window, int width, int height);//回调函数原型声明
//void processInput(GLFWwindow *window);
//
//// settings
//const unsigned int SCR_WIDTH = 800;
//const unsigned int SCR_HEIGHT = 600;
//
//
//int main(int argc, char * argv[]) {
//
//    //初始化GLFW
//    glfwInit();
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
//#ifdef __APPLE__
//    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
//#endif
//    
//    /*
//     glBufferData是一个专门用来把用户定义的数据复制到当前绑定缓冲的函数。
//     它的第一个参数是目标缓冲的类型：顶点缓冲对象当前绑定到GL_ARRAY_BUFFER目标上。第二个参数指定传输数据的大小(以字节为单位)；用一个简单的sizeof计算出顶点数据大小就行。第三个参数是我们希望发送的实际数据。
//
//     第四个参数指定了我们希望显卡如何管理给定的数据。它有三种形式：
//
//     GL_STATIC_DRAW ：数据不会或几乎不会改变。
//     GL_DYNAMIC_DRAW：数据会被改变很多。
//     GL_STREAM_DRAW ：数据每次绘制时都会改变。
//     */
//
//
//    //创建一个窗口对象
//    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "FirstGL", NULL, NULL);
//    if (window == NULL)
//    {
//        std::cout << "Failed to create GLFW window" << std::endl;
//        glfwTerminate();
//        return -1;
//    }
//
//    //通知GLFW将我们窗口的上下文设置为当前线程的主上下文
//    glfwMakeContextCurrent(window);
//
//    //对窗口注册一个回调函数,每当窗口改变大小，GLFW会调用这个函数并填充相应的参数供你处理
//    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
//
//    //初始化GLAD用来管理OpenGL的函数指针
//    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
//    {
//        std::cout << "Failed to initialize GLAD" << std::endl;
//        return -1;
//    }
//    
//    
//    
//    const GLchar* vertexShaderSource = "#version 330 core\n"
//        "layout (location = 0) in vec3 position;\n"
//        "void main()\n"
//        "{\n"
//        "gl_Position = vec4(position.x, position.y, position.z, 1.0);\n"
//        "}\0";
//    const GLchar* fragmentShaderSource = "#version 330 core\n"
//        "out vec4 color;\n"
//        "void main()\n"
//        "{\n"
//        "color = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
//        "}\n\0";
//    
//    GLuint vertexShader;
//    vertexShader = glCreateShader(GL_VERTEX_SHADER);
//    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
//    glCompileShader(vertexShader);
//    GLint success;
//    GLchar infoLog[512];
//    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
//    if(!success)
//    {
//        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
//        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
//    }
//    GLuint fragmentShader;
//    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
//    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
//    glCompileShader(fragmentShader);
//    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
//    if(!success)
//    {
//        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
//        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
//    }
//    GLuint shaderProgram;
//    shaderProgram = glCreateProgram();
//    glAttachShader(shaderProgram, vertexShader);
//    glAttachShader(shaderProgram, fragmentShader);
//    glLinkProgram(shaderProgram);
//    glDeleteShader(vertexShader);
//    glDeleteShader(fragmentShader);
//    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
//    if(!success) {
//        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
//        std::cout << "ERROR::SHADER::glLinkProgram::LINK_FAILED\n" << infoLog << std::endl;
//    }
//     //*/
//
//   
//    
//    
//    
//
//
//    GLfloat vertices1[] = {
//        1.0f, 0.5f, 0.0f,   // 右上角
//        1.0f, -0.5f, 0.0f,  // 右下角
//        -0.0f, -0.0f, 0.0f, // 左下角
//        
//       
//       
//    };
//
//    GLfloat vertices2[] = {
//        0.0f, 0.5f, 0.0f,   // 右上角
//        0.0f, -0.5f, 0.0f,  // 右下角
//        -1.0f, -0.0f, 0.0f, // 左下角
//    };
//
//
//    GLuint indices1[] = { // 注意索引从0开始!
//        0, 1, 2, // 第一个三角形
//    };
//
//
//    GLuint indices2[] = { // 注意索引从0开始!
//        0, 1, 2, // 第一个三角形
//    };
//
//    
//    //*
//    GLuint VAO1;
//    //生成并绑定顶点缓冲对象 vertex buffer objects
//
//    //*
//    glGenVertexArrays(1, &VAO1);//function name 有 s 说明是可以生成多个的，这里只有一个，所以直接取地址作为第一个元素。
//
//    // 1. 绑定VAO
//    glBindVertexArray(VAO1);
//    // 0. 复制顶点数组到缓冲中供OpenGL使用
//    GLuint VBO1;
//    glGenBuffers(1, &VBO1);
//    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
//    //从这一刻起，我们使用的任何（在GL_ARRAY_BUFFER目标上的）缓冲调用都会用来配置当前绑定的缓冲(VBO)。然后我们可以调用glBufferData函数，它会把之前定义的顶点数据复制到缓冲的内存中：
//    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);
//    
//    
//    GLuint EBO1;
//    glGenBuffers(1, &EBO1);
//    
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO1);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices1), indices1, GL_STATIC_DRAW);
//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
//    //这里的第一个参数0就是和shader代码里面的location关联的。
//    glEnableVertexAttribArray(0);//必须要开启
//    
//    glBindBuffer(GL_ARRAY_BUFFER, 0);
//    glBindVertexArray(0);
//    //*/
//    
//    GLuint VAO2;
//    glGenVertexArrays(1, &VAO2);//function name 有 s 说明是可以生成多个的，这里只有一个，所以直接取地址作为第一个元素。
//    glBindVertexArray(VAO2);
//    // 0. 复制顶点数组到缓冲中供OpenGL使用
//    GLuint VBO2;
//    glGenBuffers(1, &VBO2);
//    glBindBuffer(GL_ARRAY_BUFFER, VBO2);
////    //从这一刻起，我们使用的任何（在GL_ARRAY_BUFFER目标上的）缓冲调用都会用来配置当前绑定的缓冲(VBO)。然后我们可以调用glBufferData函数，它会把之前定义的顶点数据复制到缓冲的内存中：
//    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);
//
//    GLuint EBO2;
//    glGenBuffers(1, &EBO2);
//
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO2);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices2), indices2, GL_STATIC_DRAW);
//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
//    glEnableVertexAttribArray(0);//必须要开启
//    glBindVertexArray(0);
//
//    /*
//     顶点属性指针告诉 每个顶点属性从一个VBO管理的内存中获得它的数据，而具体是从哪个VBO（程序中可以有多个VBO）获取则是通过在调用glVetexAttribPointer时绑定到GL_ARRAY_BUFFER的VBO决定的
//     
//     // 1. 设置顶点属性指针
//         第一个参数指定我们要配置的顶点属性。还记得我们在顶点着色器中使用layout(location = 0)定义了position顶点属性的位置值(Location)吗？它可以把顶点属性的位置值设置为0。因为我们希望把数据传递到这一个顶点属性中，所以这里我们传入0。
//         第二个参数指定顶点属性的大小。顶点属性是一个vec3，它由3个值组成，所以大小是3。
//         第三个参数指定数据的类型，这里是GL_FLOAT(GLSL中vec*都是由浮点数值组成的)。
//         下个参数定义我们是否希望数据被标准化(Normalize)。如果我们设置为GL_TRUE，所有数据都会被映射到0（对于有符号型signed数据是-1）到1之间。我们把它设置为GL_FALSE。
//         第五个参数叫做步长(Stride)，它告诉我们在连续的顶点属性组之间的间隔。由于下个组位置数据在3个GLfloat之后，我们把步长设置为3 * sizeof(GLfloat)。要注意的是由于我们知道这个数组是紧密排列的（在两个顶点属性之间没有空隙）我们也可以设置为0来让OpenGL决定具体步长是多少（只有当数值是紧密排列时才可用）。一旦我们有更多的顶点属性，我们就必须更小心地定义每个顶点属性之间的间隔，我们在后面会看到更多的例子(译注: 这个参数的意思简单说就是从这个属性第二次出现的地方到整个数组0位置之间有多少字节)。
//         最后一个参数的类型是GLvoid*，所以需要我们进行这个奇怪的强制类型转换。它表示位置数据在缓冲中起始位置的偏移量(Offset)。由于位置数据在数组的开头，所以这里是0。我们会在后面详细解释这个参数。
//     */
//    
//    
//    
//    
//    
//    
//    glUseProgram(shaderProgram);
//    //渲染循环
//    while(!glfwWindowShouldClose(window))
//    {
//        // 输入
//        processInput(window);
//
//        // 渲染指令
//        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
//        glClear(GL_COLOR_BUFFER_BIT);
//
//        
//
//        // 2. 当我们渲染一个物体时要使用着色器程序
//        
//        glBindVertexArray(VAO1);
//        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//        glBindBuffer(GL_ARRAY_BUFFER, VBO1);
////        glDrawArrays(GL_TRIANGLES, 0, 3);
//
//        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO1);
//        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);
//
//        glBindVertexArray(0);
//        glBindVertexArray(VAO2);
////        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
////        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//        glBindBuffer(GL_ARRAY_BUFFER, VBO2);
////        //        glDrawArrays(GL_TRIANGLES, 0, 3);
////
//        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO2);
//        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);
//        glBindVertexArray(0);
//        glBindBuffer(GL_ARRAY_BUFFER, 0);
//        
//        // 检查并调用事件，交换缓冲
//        glfwSwapBuffers(window);//检查触发事件
//        glfwPollEvents();    //交换颜色缓冲
//    }
//    
//    
//    
//    
//    
//    
//    // Properly de-allocate all resources once they've outlived their purpose
////    glDeleteVertexArrays(1, &VAO1);
////    glDeleteBuffers(1, &VBO1);
//    glDeleteVertexArrays(1, &VAO2);
//    glDeleteBuffers(1, &VBO2);
//    // Terminate GLFW, clearing any resources allocated by GLFW.
//
//    //释放/删除之前的分配的所有资源
//    glfwTerminate();
//    return EXIT_SUCCESS;
//}
//
////输入控制，检查用户是否按下了返回键(Esc)
//void processInput(GLFWwindow *window)
//{
//    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
//        glfwSetWindowShouldClose(window, true);
//}
//
//// 当用户改变窗口的大小的时候，视口也应该被调整
//void framebuffer_size_callback(GLFWwindow* window, int width, int height)
//{
//    // 注意：对于视网膜(Retina)显示屏，width和height都会明显比原输入值更高一点。
//    glViewport(0, 0, width, height);
//}
//
////*/
//
