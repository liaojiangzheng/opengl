////
////  main.cpp
////  openglproject3
////
////  Created by 廖江正 on 2020/12/7.
////  Copyright © 2020 廖江正. All rights reserved.
////
//
//#include "shader1.hpp"
////#include "test.hpp"
//void framebuffer_size_callback(GLFWwindow* window, int width, int height);//回调函数原型声明
//void processInput(GLFWwindow *window);
//
//// settings
//const unsigned int SCR_WIDTH = 800;
//const unsigned int SCR_HEIGHT = 600;
//
////输入控制，检查用户是否按下了返回键(Esc)
//void processInput(GLFWwindow *window)
//{
//    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
//        glfwSetWindowShouldClose(window, true);
//}
//
//// 当用户改变窗口的大小的时候，视口也应该被调整
//void framebuffer_size_callback(GLFWwindow* window, int width, int height)
//{
//    // 注意：对于视网膜(Retina)显示屏，width和height都会明显比原输入值更高一点。
//    glViewport(0, 0, width, height);
//}
//
//int main(int argc, char * argv[]) {
//
//    //初始化GLFW
//    glfwInit();
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
//#ifdef __APPLE__
//    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
//#endif
//
//    //创建一个窗口对象
//    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Texture", NULL, NULL);
//    if (window == NULL)
//    {
//        std::cout << "Failed to create GLFW window" << std::endl;
//        glfwTerminate();
//        return -1;
//    }
//
//    //通知GLFW将我们窗口的上下文设置为当前线程的主上下文
//    glfwMakeContextCurrent(window);
//
//    //对窗口注册一个回调函数,每当窗口改变大小，GLFW会调用这个函数并填充相应的参数供你处理
//    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
//
//    //初始化GLAD用来管理OpenGL的函数指针
//    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
//    {
//        std::cout << "Failed to initialize GLAD" << std::endl;
//        return -1;
//    }
//
////    Shader1* ourshader = new Shader1("openglproject3/shader1/shadersrc/vertexshader1.txt", "shader1/shadersrc/fragmentshader1.txt");
//    Shader1* ourshader = new Shader1("/Users/liaojiangzheng/work/opengl/openglproject3/openglproject3/openglproject3/shadertexture/shadersrc/vertexshader1.txt", "/Users/liaojiangzheng/work/opengl/openglproject3/openglproject3/openglproject3/shadertexture/shadersrc/fragmentshader1.txt");
//    
//    
//
//    
//   GLfloat vertices11[] = {
//        // 位置              // 颜色
//         0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,   // 右下
//        -0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,   // 左下
//         0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f    // 顶部
//    };
//
//    
//    GLfloat vertices1[] = {
//    //     ---- 位置 ----       ---- 颜色 ----     - 纹理坐标 -
//         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // 右上
//         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // 右下
//        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // 左下
//        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // 左上
//    };
//    
//
//    GLuint indices1[] = { // 注意索引从0开始!
//        0, 1, 2, // 第一个三角形
//    };
//
//
//    //*
//    GLuint VAO;
//    //生成并绑定顶点缓冲对象 vertex buffer objects
//
//    //*
//    glGenVertexArrays(1, &VAO);//function name 有 s 说明是可以生成多个的，这里只有一个，所以直接取地址作为第一个元素。
//    // 1. 绑定VAO
//    glBindVertexArray(VAO);
//    // 1. 复制顶点数组到缓冲中供OpenGL使用
//    GLuint VBO;
//    glGenBuffers(1, &VBO);
//    glBindBuffer(GL_ARRAY_BUFFER, VBO);
//
//    //从这一刻起，我们使用的任何（在GL_ARRAY_BUFFER目标上的）缓冲调用都会用来配置当前绑定的缓冲(VBO)。然后我们可以调用glBufferData函数，它会把之前定义的顶点数据复制到缓冲的内存中：
//    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);
//    GLuint EBO;
//    glGenBuffers(1, &EBO);
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices1), indices1, GL_STATIC_DRAW);
//    
//    /*
//     配置第0个顶点属性
//     定点属性的大小是3，由3个值组成
//     顶点属性的每一个值是float类型
//     数据是否被标准化
//     步长为6 * sizeof(GLfloat)
//     位置数据在缓冲中起始位置的偏移量(Offset)
//     */
//    //0这个index就是position数据
//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
//    //这里的第一个参数0就是和shader代码里面的location关联的。
//    glEnableVertexAttribArray(0);//必须要开启
//    
//    //1这个index就是颜色属性
//    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8* sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
//    glEnableVertexAttribArray(1);//必须要开启
//    
//    //2这个index就是纹理坐标属性，长度是2，类型是float，不标准化，步长是8，偏移6
//    glVertexAttribPointer(2, 2, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
//    glEnableVertexAttribArray(2);
//
//
////////////////////////////////////////////////////////////////////////////////纹理处理///////////////////////////////////////////////////////////////////////////////////////////////////////////////
////    glBindBuffer(GL_ARRAY_BUFFER, 0);
//    glBindVertexArray(0);
//    
//    //1.生成纹理
//    GLuint texture1;
//    /*
//     1、生成纹理的数量
//     2、储存在第二个参数的GLuint数组中
//     */
//    glGenTextures(1, &texture1);
//    
//    /*
//    2、绑定纹理
//    */
//    glBindTexture(GL_TEXTURE_2D, texture1);
//    
//    //使用glUniform1i设置uniform采样器的位置值，或者说纹理单元。
////    glUniform1i(glGetUniformLocation(ourshader->Program, "ourTexture1"), 0);//
//    
//    /*
//     3、指定纹理的图片内容
//     */
//    int width, height;
//    unsigned char* image = SOIL_load_image("/Users/liaojiangzheng/Desktop/均乐公章3.png", &width, &height, 0, SOIL_LOAD_RGB);
//    std::cout << "width, height: " <<width<<","<< height << std::endl;
//    /*
//     1、第一个参数指定了纹理目标(Target)。设置为GL_TEXTURE_2D意味着会生成与当前绑定的纹理对象在同一个目标上的纹理（任何绑定到GL_TEXTURE_1D和GL_TEXTURE_3D的纹理不会受到影响）。
//     2、第二个参数为纹理指定多级渐远纹理的级别，如果你希望单独手动设置每个多级渐远纹理的级别的话。这里我们填0，也就是基本级别。
//     3、第三个参数告诉OpenGL我们希望把纹理储存为何种格式。我们的图像只有RGB值，因此我们也把纹理储存为RGB值。
//     4、第四个和第五个参数设置最终的纹理的宽度和高度。我们之前加载图像的时候储存了它们，所以我们使用对应的变量。
//     5、下个参数应该总是被设为0（历史遗留问题）。
//     6、第七第八个参数定义了源图的格式和数据类型。我们使用RGB值加载这个图像，并把它们储存为char(byte)数组，我们将会传入对应值。
//     7、最后一个参数是真正的图像数据。
//     */
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
//    glGenerateMipmap(GL_TEXTURE_2D);
//    
//    /*
//     4、最后释放图片，和解绑纹理。
//     */
//    SOIL_free_image_data(image);
//    glBindTexture(GL_TEXTURE_2D, 0);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    
//    
//    
//    
//
//   
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);    // Set texture wrapping to GL_REPEAT (usually basic wrapping method)
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//    // Set texture filtering parameters
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//    
//   
//    
//
//    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    GLuint texture2;
//    glGenTextures(1, &texture2);
////    glActiveTexture(GL_TEXTURE1);//先激活GL_TEXTURE1
//    glBindTexture(GL_TEXTURE_2D, texture2);
////    glUniform1i(glGetUniformLocation(ourshader->Program, "ourTexture2"), 1);
//    int width2, height2;
//    unsigned char* image2 = SOIL_load_image("/Users/liaojiangzheng/Desktop/商标注册.jpg", &width2, &height2, 0, SOIL_LOAD_RGB);
//    std::cout << "width2, height2: " <<width2<<","<< height2 << std::endl;
//    /*
//     第一个参数指定了纹理目标(Target)。设置为GL_TEXTURE_2D意味着会生成与当前绑定的纹理对象在同一个目标上的纹理（任何绑定到GL_TEXTURE_1D和GL_TEXTURE_3D的纹理不会受到影响）。
//     第二个参数为纹理指定多级渐远纹理的级别，如果你希望单独手动设置每个多级渐远纹理的级别的话。这里我们填0，也就是基本级别。
//     第三个参数告诉OpenGL我们希望把纹理储存为何种格式。我们的图像只有RGB值，因此我们也把纹理储存为RGB值。
//     第四个和第五个参数设置最终的纹理的宽度和高度。我们之前加载图像的时候储存了它们，所以我们使用对应的变量。
//     下个参数应该总是被设为0（历史遗留问题）。
//     第七第八个参数定义了源图的格式和数据类型。我们使用RGB值加载这个图像，并把它们储存为char(byte)数组，我们将会传入对应值。
//     最后一个参数是真正的图像数据。
//     */
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width2, height2, 0, GL_RGB, GL_UNSIGNED_BYTE, image2);
//    glGenerateMipmap(GL_TEXTURE_2D);
//    
//    SOIL_free_image_data(image2);
//    glBindTexture(GL_TEXTURE_2D, 0);
//    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    
////    GLint nrAttributes;
////    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
////    std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;
//
//    //渲染循环
//    while(!glfwWindowShouldClose(window))
//    {
//        // 输入
//        processInput(window);
//
//        // 渲染指令
//        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
//        glClear(GL_COLOR_BUFFER_BIT);
//
//
//
//
////在CPU里面去设置uniform变量的值，循环设置，从而在渲染的时候会得到不同的值。
////        GLfloat timeValue = glfwGetTime();
////        GLfloat greenValue = (sin(timeValue) / 2) + 0.5;
////        GLint vertexColorLocation = glGetUniformLocation(ourshader->Program, "uniformColor");
////
////        glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f);
////        ourshader->Use();
//        // 2. 当我们渲染一个物体时要使用着色器程序
////        glUseProgram(shaderProgram1);
////        glBindVertexArray(VAO);
////        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
////        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
////        glBindBuffer(GL_ARRAY_BUFFER, VBO);
////        glDrawArrays(GL_TRIANGLES, 0, 3);
//
//
////        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
////        glBindTexture(GL_TEXTURE_2D, texture1);
//        ourshader->Use();
//        
//        
//        //在绑定纹理之前先激活纹理单元，激活纹理单元之后，接下来的glBindTexture函数调用会绑定这个纹理到当前激活的纹理单元，纹理单元GL_TEXTURE0默认总是被激活
//        glActiveTexture(GL_TEXTURE0);//1、激活0这个单元
//        glBindTexture(GL_TEXTURE_2D, texture1);//2、绑定到0这个单元
//        glUniform1i(glGetUniformLocation(ourshader->Program, "ourTexture1"), 0);//3、从0这个单元去取
//        
//        glActiveTexture(GL_TEXTURE1);
//        glBindTexture(GL_TEXTURE_2D, texture2);
//        glUniform1i(glGetUniformLocation(ourshader->Program, "ourTexture2"), 1);
//        
//        glBindVertexArray(VAO);
//        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
//        glBindVertexArray(0);
////        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);
//
////        glDrawArrays(GL_TRIANGLES, 0, 3);
////        glBindVertexArray(0);
//
//        // 检查并调用事件，交换缓冲
//        glfwSwapBuffers(window);//检查触发事件
//        glfwPollEvents();    //交换颜色缓冲
//    }
//
//
//    glDeleteVertexArrays(1, &VAO);
//    glDeleteBuffers(1, &VBO);
//    // Terminate GLFW, clearing any resources allocated by GLFW.
//
//    //释放/删除之前的分配的所有资源
//    glfwTerminate();
//    return EXIT_SUCCESS;
//}
