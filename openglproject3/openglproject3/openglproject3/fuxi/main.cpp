//
//  main.cpp
//  openglproject3
//
//  Created by 廖江正 on 2020/12/15.
//  Copyright © 2020 廖江正. All rights reserved.
//

#include "shaderclass.hpp"

const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
GLfloat aspect =  45.0f;
glm::vec3 lightColor(1.0f, 1.0f, 1.0f);
glm::vec3 toyColor(1.0f, 0.5f, 0.31f);
glm::vec3 result = lightColor * toyColor; // = (1.0f, 0.5f, 0.31f);
glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);
GLfloat deltaTime = 0.0f;   // 当前帧遇上一帧的时间差
GLfloat lastFrame = 0.0f;   // 上一帧的时间
bool keys[1024];
GLfloat yaw   = -90.0f;    // Yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right (due to how Eular angles work) so we initially rotate a bit to the left.
GLfloat pitch =   0.0f;
GLfloat lastX =  SCR_WIDTH  / 2.0;
GLfloat lastY =  SCR_HEIGHT / 2.0;
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));


// Function prototypes
void framebuffer_size_callback(GLFWwindow* window, int width, int height);//回调函数原型声明
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void do_movement();
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

int main(int argc, char * argv[]) {
    //////////////////////////////////////////////////////////////////////////////创建窗口固定流程START///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //初始化GLFW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif
    //创建一个窗口对象
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "祖师爷在此", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    //通知GLFW将我们窗口的上下文设置为当前线程的主上下文
    glfwMakeContextCurrent(window);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(window, mouse_callback);//鼠标移动回调函数
    glfwSetKeyCallback(window, key_callback);//设置键盘按钮回调函数// Set the required callback functions
    glfwSetScrollCallback(window, scroll_callback);//鼠标滚轮滚动函数
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);//对窗口注册一个回调函数,每当窗口改变大小，GLFW会调用这个函数并填充相应的参数供你处理
    
    //初始化GLAD用来管理OpenGL的函数指针
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }
    //////////////////////////////////////////////////////////////////////////////创建窗口固定流程END///////////////////////////////////////////////////////////////////////////////////////////////////////////////




    //////////////////////////////////////////////////////////////////////////////SHADER固定流程START///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ShaderClass* ourshader = new ShaderClass("/Users/liaojiangzheng/work/opengl/openglproject3/openglproject3/openglproject3/fuxi/shadersrc/vertexshader.txt", "/Users/liaojiangzheng/work/opengl/openglproject3/openglproject3/openglproject3/fuxi/shadersrc/fragmentshader.txt");

    float vertices1[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    };

    //索引数组，表示用第几个点的数据去绘制。顶点索引
    GLuint indices1[] = { // 注意索引从0开始!
        0, 1, 2, // 第一个三角形
    };

    /*
     顶点数组
     一般当你打算绘制多个物体时，你首先要生成/配置所有的VAO（和必须的VBO及属性指针)，然后储存它们供后面使用。当我们打算绘制物体的时候就拿出相应的VAO，绑定它，绘制完物体后，再解绑VAO。
     //设置好这个VAO的内容之后，要解绑VAO，然后去绑定另一个VAO，然后设置它，之后再解绑，如此下去直到最后的VAO都设置完了。 使用的时候在绘制之前要使用那个VAO就绑定那个VAO即可，使用完了记得解绑。
     */
    GLuint VAO;
    glGenVertexArrays(1, &VAO);//function name 有 s 说明是可以生成多个的，这里只有一个，所以直接取地址作为第一个元素。
    // 1. 绑定VAO
    glBindVertexArray(VAO);
    
    // 1. 复制顶点数组到缓冲中供OpenGL使用
    GLuint VBO;//顶点缓冲对象的编号(缓冲对象ID)
    glGenBuffers(1, &VBO);//产生一个缓冲对象,和缓冲对象ID关联起来，通过缓冲对象ID去使用缓冲对象。
    glBindBuffer(GL_ARRAY_BUFFER, VBO);//将缓冲对象绑定到顶点缓冲类型(GL_ARRAY_BUFFER)目标上面去，以后去去设置/使用顶点缓冲类型目标的数据都是使用的这个VBO数据。

    //从这一刻起，我们使用的任何（在GL_ARRAY_BUFFER目标上的）缓冲调用都会用来配置当前绑定的缓冲(VBO)。
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);//调用glBufferData函数，把之前定义的顶点数据复制到缓冲的内存中（当前绑定的缓冲）。
    
    //如何将顶点数据链接到顶点着色器的属性上???
    /*
     告诉opengl如何解析数据，就是告诉顶点缓冲对象数组的内容的构成。设置的是当前绑定到GL_ARRAY_BUFFER上的VBO对象。
     配置第0个顶点属性，和layout(location = 0)中的0对应。
     定点属性的大小是3，由3个值组成
     顶点属性的每一个值是float类型
     数据是否被标准化
     步长为6 * sizeof(GLfloat)
     位置数据在缓冲中起始位置的偏移量(Offset)
    */
    //0这个index就是position数据
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
    //这里的第一个参数0就是和shader代码里面的location关联的。
    glEnableVertexAttribArray(0);//必须要开启，以顶点属性值作为参数，启用0这个顶点属性。
    
    /*
     索引缓冲对象，专门存储索引。
     把索引数组关联到GL_ELEMENT_ARRAY_BUFFER，而GL_ELEMENT_ARRAY_BUFFER又是和EBO关联的，所以以后用EBO就可以
     
     绘制的时候使用专门的绘制函数->glDrawElements：指明我们从索引缓冲渲染。
    */
    GLuint EBO;
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices1), indices1, GL_STATIC_DRAW);
    
    //1这个index就是颜色属性
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8* sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(1);//必须要开启

    //2这个index就是纹理坐标属性，长度是2，类型是float，不标准化，步长是8，偏移6
    glVertexAttribPointer(2, 2, GL_FLOAT,GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    glBindVertexArray(0);//设置好这个VAO的内容之后，要解绑VAO，然后去绑定另一个VAO，然后设置它，之后再解绑，如此下去直到最后的VAO都设置完了。 使用的时候在绘制之前要使用那个VAO就绑定那个VAO即可，使用完了记得解绑。

//////////////////////////////////////////////////////////////////////////////纹理处理///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    glBindBuffer(GL_ARRAY_BUFFER, 0);


    //1.生成纹理
    GLuint texture1;
    /*
     1、生成纹理的数量
     2、储存在第二个参数的GLuint数组中
     */
    glGenTextures(1, &texture1);

    /*
    2、绑定纹理
    */
    glBindTexture(GL_TEXTURE_2D, texture1);

    //使用glUniform1i设置uniform采样器的位置值，或者说纹理单元。
//    glUniform1i(glGetUniformLocation(ourshader->Program, "ourTexture1"), 0);//

    /*
     3、指定纹理的图片内容
     */
    int width, height;
    unsigned char* image = SOIL_load_image("/Users/liaojiangzheng/Desktop/均乐公章3.png", &width, &height, 0, SOIL_LOAD_RGB);
    std::cout << "width, height: " <<width<<","<< height << std::endl;
    /*
     1、第一个参数指定了纹理目标(Target)。设置为GL_TEXTURE_2D意味着会生成与当前绑定的纹理对象在同一个目标上的纹理（任何绑定到GL_TEXTURE_1D和GL_TEXTURE_3D的纹理不会受到影响）。
     2、第二个参数为纹理指定多级渐远纹理的级别，如果你希望单独手动设置每个多级渐远纹理的级别的话。这里我们填0，也就是基本级别。
     3、第三个参数告诉OpenGL我们希望把纹理储存为何种格式。我们的图像只有RGB值，因此我们也把纹理储存为RGB值。
     4、第四个和第五个参数设置最终的纹理的宽度和高度。我们之前加载图像的时候储存了它们，所以我们使用对应的变量。
     5、下个参数应该总是被设为0（历史遗留问题）。
     6、第七第八个参数定义了源图的格式和数据类型。我们使用RGB值加载这个图像，并把它们储存为char(byte)数组，我们将会传入对应值。
     7、最后一个参数是真正的图像数据。
     */
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);
    /*
     4、最后释放图片，和解绑纹理。
     */
    SOIL_free_image_data(image);
    glBindTexture(GL_TEXTURE_2D, 0);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);    // Set texture wrapping to GL_REPEAT (usually basic wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    GLuint texture2;
    glGenTextures(1, &texture2);
//    glActiveTexture(GL_TEXTURE1);//先激活GL_TEXTURE1
    glBindTexture(GL_TEXTURE_2D, texture2);
//    glUniform1i(glGetUniformLocation(ourshader->Program, "ourTexture2"), 1);
    int width2, height2;
    unsigned char* image2 = SOIL_load_image("/Users/liaojiangzheng/Desktop/商标注册.jpg", &width2, &height2, 0, SOIL_LOAD_RGB);
    std::cout << "width2, height2: " <<width2<<","<< height2 << std::endl;
    /*
     第一个参数指定了纹理目标(Target)。设置为GL_TEXTURE_2D意味着会生成与当前绑定的纹理对象在同一个目标上的纹理（任何绑定到GL_TEXTURE_1D和GL_TEXTURE_3D的纹理不会受到影响）。
     第二个参数为纹理指定多级渐远纹理的级别，如果你希望单独手动设置每个多级渐远纹理的级别的话。这里我们填0，也就是基本级别。
     第三个参数告诉OpenGL我们希望把纹理储存为何种格式。我们的图像只有RGB值，因此我们也把纹理储存为RGB值。
     第四个和第五个参数设置最终的纹理的宽度和高度。我们之前加载图像的时候储存了它们，所以我们使用对应的变量。
     下个参数应该总是被设为0（历史遗留问题）。
     第七第八个参数定义了源图的格式和数据类型。我们使用RGB值加载这个图像，并把它们储存为char(byte)数组，我们将会传入对应值。
     最后一个参数是真正的图像数据。
     */
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width2, height2, 0, GL_RGB, GL_UNSIGNED_BYTE, image2);
    glGenerateMipmap(GL_TEXTURE_2D);
    SOIL_free_image_data(image2);
    glBindTexture(GL_TEXTURE_2D, 0);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    glEnable(GL_DEPTH_TEST);
    glm::vec3 cubePositions[] = {
      glm::vec3( 0.0f,  0.0f,  0.0f),
      glm::vec3( 2.0f,  5.0f, -15.0f),
      glm::vec3(-1.5f, -2.2f, -2.5f),
      glm::vec3(-3.8f, -2.0f, -12.3f),
      glm::vec3( 2.4f, -0.4f, -3.5f),
      glm::vec3(-1.7f,  3.0f, -7.5f),
      glm::vec3( 1.3f, -2.0f, -2.5f),
      glm::vec3( 1.5f,  2.0f, -2.5f),
      glm::vec3( 1.5f,  0.2f, -1.5f),
      glm::vec3(-1.3f,  1.0f, -1.5f)
    };
    //*
    //渲染循环
    while(!glfwWindowShouldClose(window))
    {
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        do_movement();
        // 渲染指令
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glm::mat4 model = glm::mat4(1.0f);
//      model = glm::rotate(model, glm::radians(-45.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::rotate(model, (GLfloat)glfwGetTime() * glm::radians(50.0f), glm::vec3(0.5f, 1.0f, 0.0f));
//      glm::mat4 view = glm::mat4(1.0f);
        // 注意，我们将矩阵向我们要进行移动场景的反向移动。
//      view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
//      GLfloat radius = 10.0f;
//      GLfloat camX = sin(glfwGetTime()) * radius;
//      GLfloat camZ = cos(glfwGetTime()) * radius;
        glm::mat4 view;
        view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
//        view = glm::lookAt(glm::vec3(camX, 0.0, camZ), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));
        glm::mat4 projection;
//        projection = glm::perspective(45.0f, (GLfloat)SCR_WIDTH / (GLfloat)SCR_HEIGHT, 0.1f, 100.0f);
        projection = glm::perspective(aspect, (GLfloat)SCR_WIDTH/(GLfloat)SCR_HEIGHT, 0.1f, 100.0f);
//        glm::mat4 view;
         view = camera.GetViewMatrix();
//         glm::mat4 projection;
         projection = glm::perspective(camera.Zoom, (float)SCR_WIDTH/(float)SCR_HEIGHT, 0.1f, 1000.0f);
        GLint modelLoc = glGetUniformLocation(ourshader->Program, "model");
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        GLint viewLoc = glGetUniformLocation(ourshader->Program, "view");
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        GLint projectionLoc = glGetUniformLocation(ourshader->Program, "projection");
        glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
        ourshader->Use();

        //在绑定纹理之前先激活纹理单元，激活纹理单元之后，接下来的glBindTexture函数调用会绑定这个纹理到当前激活的纹理单元，纹理单元GL_TEXTURE0默认总是被激活
        glActiveTexture(GL_TEXTURE0);//1、激活0这个单元
        glBindTexture(GL_TEXTURE_2D, texture1);//2、绑定到0这个单元
        glUniform1i(glGetUniformLocation(ourshader->Program, "ourTexture1"), 0);//3、从0这个单元去取
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);
        glUniform1i(glGetUniformLocation(ourshader->Program, "ourTexture2"), 1);
        glBindVertexArray(VAO);
        for(GLuint i = 0; i < 10; i++)
        {
          glm::mat4 model = glm::mat4(1.0f);
          model = glm::translate(model, cubePositions[i]);
          GLfloat angle = (GLfloat)glfwGetTime() * glm::radians(50.0f);
          model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));
          GLint modelLoc = glGetUniformLocation(ourshader->Program, "model");
          glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
          glDrawArrays(GL_TRIANGLES, 0, 36);
        }
        
        glBindVertexArray(0);
        // 检查并调用事件，交换缓冲
        glfwSwapBuffers(window);//检查触发事件
        glfwPollEvents();    //交换颜色缓冲
    }
     //*/


    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    // Terminate GLFW, clearing any resources allocated by GLFW.

    //释放/删除之前的分配的所有资源
    glfwTerminate();
    return EXIT_SUCCESS;
}



// 当用户改变窗口的大小的时候，视口也应该被调整
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // 注意：对于视网膜(Retina)显示屏，width和height都会明显比原输入值更高一点。
    glViewport(0, 0, width, height);//标准化设备坐标接着会变换为屏幕空间坐标(Screen-space Coordinates)，这是使用你通过glViewport函数提供的数据，进行视口变换(Viewport Transform)完成的。
}

/*
 鼠标滚动响应函数
 */
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);

}
/*
 键盘输入响应函数
 */
// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    if (key >= 0 && key < 1024)
    {
        if (action == GLFW_PRESS)
            keys[key] = true;
        else if (action == GLFW_RELEASE)
            keys[key] = false;
    }
}

/*
 执行移动函数，死循环里面一直调用它，然后就会每次去判断这些if 里面的keys是否满足满足则执行。
 */
void do_movement()
{
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

/*
 鼠标移动的响应函数
 */
bool firstMouse = true;
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left
    
    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}
